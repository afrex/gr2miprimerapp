//
//  BackendManager.swift
//  MiPrimerAppGr2
//
//  Created by alexfb on 5/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

class BackendManager{
    func consultarClimaPorCiudad(ciudad:String,completionHandler:@escaping(String)->()){
        
        //let ciudad=ciudadTextField.text ?? "quito"
        let appid="12832b1c3bcef01ad30a8b6dac37a823"
        let urlString="http://api.openweathermap.org/data/2.5/weather?q=\(ciudad)&appid=\(appid)"
        
        let url=URL(string: urlString)
        
        let task=URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error != nil{
                print("El error es: \(error!)")
                return
            }
            // print(response)
            guard let dataResponse = data else{
                print("No hay datos")
                return
            }
            print(dataResponse)
            do{
                let JSONData=try JSONSerialization.jsonObject(with: dataResponse, options: [])
                //print(JSONData)
                //print(type(of: JSONData))
                let jsonDict=JSONData as! NSDictionary
                let weatherArray=jsonDict["weather"] as! NSArray
                //print(jsonDict["weather"])
                //print(weatherArray[0])
                //print(type(of: weatherArray[0]))
                let weatherDict=weatherArray[0] as! NSDictionary
                completionHandler((weatherDict["main"] as? String)!)
                //print(weatherDict["main"])
               // DispatchQueue.main.async {
                    //self.resultadoLabel.text=(weatherDict["main"] as? String)
                //}
                
            } catch{
                print("Error al generar JSON")
                return
            }
            
        }
        task.resume()

    }
    func consultarClimaPorUbicacion(coordenadas:CLLocationCoordinate2D){
        let appid="12832b1c3bcef01ad30a8b6dac37a823"
        let urlString="http://api.openweathermap.org/data/2.5/weather?lat=\(coordenadas.latitude)&lon=\(coordenadas.longitude)&appid=\(appid)"
        Alamofire.request(urlString).responseJSON { response in
            debugPrint(response)
            
            guard let json = response.result.value as? NSDictionary else{
                print("error")
                return
            }
            let weatherArray=json["weather"] as! NSArray
    
            let weatherDict=weatherArray[0] as! NSDictionary
            let clima="\(weatherDict["main"]!)"
            print(clima)
            NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil, userInfo: ["clima":clima])
            //print(weatherDict["main"] as? String)
        }
    
    }
}
