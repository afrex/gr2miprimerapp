//
//  GuestViewController.swift
//  MiPrimerAppGr2
//
//  Created by alexfb on 5/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import CoreLocation

class GuestViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var bienvenidoLabel: UILabel!
    @IBOutlet weak var climaLabel: UILabel!
    
    let locationManager=CLLocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate=self
        locationManager.requestAlwaysAuthorization()
        
        //print(locationManager.location?.coordinate)
        guard let coordenadas=locationManager.location?.coordinate else{
        return
        }
        //print(coordenadas)
    let bm=BackendManager()
        bm.consultarClimaPorUbicacion(coordenadas:coordenadas)
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("actualizar"), object: nil)
    }
    
    func actualizarDatos(_ notification:Notification){
    let clima=notification.userInfo?["clima"]
        //print(clima)
        climaLabel.text="\(clima)"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"), object: nil)
    }

    
}
