//
//  ViewController.swift
//  MiPrimerAppGr2
//
//  Created by alexfb on 28/4/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //siempre poner aqui los metodos las variables que voy a cambiar
    
    
    @IBOutlet weak var usuarioTextField: UITextField!
    
    @IBOutlet weak var passwordTextFiel: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


//lo que no voy a cambiar
    
    @IBAction func entrarButton(_ sender: Any) {
        
        
        
        //print(usuarioTextField.text!)
        let usuario=usuarioTextField.text ?? "user"
        
        //print(usuario)
        
        //esta es la forma mas segura de romper la opcion Optional
        guard let password=passwordTextFiel.text else {
            return
        }
        
        switch (usuario,password) {
        case ("alex","1234"):
            performSegue(withIdentifier: "toWelcomeSegue", sender: self)
            print("login correcto")
            
        default:
            print("error")
        }
    }
    
    //va a funcionar cuando toque en otro campo en la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //passwordTextFiel.resignFirstResponder()
        //usuarioTextField.resignFirstResponder()
        view.endEditing(true)
    }
    
    //para mandar informacion de una pantalla a otra
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="toWelcomeSegue"{
            let destination=segue.destination as! WelcomeViewController
            
            //esta es la forma que no debe hacer
            //destination.welcomeMessageLabel.text="Bienvenido \(usuarioTextField.text!)"
            
            //forma correcta
            destination.message="Bienvenido \(usuarioTextField.text!)"
    }
    
}
}

