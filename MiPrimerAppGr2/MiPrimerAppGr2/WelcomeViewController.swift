//
//  WelcomeViewController.swift
//  MiPrimerAppGr2
//
//  Created by alexfb on 28/4/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    
    @IBOutlet weak var welcomeMessageLabel: UILabel!
    
    @IBOutlet weak var ciudadTextField: UITextField!
    var message:String?
    
    @IBOutlet weak var resultadoLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        welcomeMessageLabel.text=message!
    }
    @IBAction func consultarButtonPressed(_ sender: Any) {
    let backend=BackendManager()
        let ciudad=ciudadTextField.text ?? "quito"
        backend.consultarClimaPorCiudad(ciudad: ciudad) { (clima) in
            DispatchQueue.main.async {
                self.resultadoLabel.text=clima
            }
        }
    }
}
